/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leer_textos;
import java.io.*;
/*import java.io.BufferedReader;
import java.io.FileReader;*/

/**
 *
 * 
 * Ernesto Lerma Carrasco
 */
public class Textos {
    public void leer(String nombreArchivo){
        try{
            FileReader r = new FileReader(nombreArchivo);
            BufferedReader buffer = new BufferedReader(r);
            System.out.println(buffer.readLine());
            
            String temp = "";
            
            while  (temp!=null)
            {
                temp=buffer.readLine();
                if (temp==null) 
                    break;
                    System.out.println(temp);
                
            };
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
