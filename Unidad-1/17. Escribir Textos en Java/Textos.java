/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escribir_texto;
import java.io.*;
/**
 *
 * 
 * Ernesto Lerma Carrasco
 */
public class Textos {
    public void escribir (String nombreArchivo){
        File f;
        f = new File(nombreArchivo);
        
        //Escritura
        try {
            FileWriter w = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(w);
            PrintWriter wr = new PrintWriter(bw);
            wr.write("Esto es una linea de texto");
            wr.append("\n Concatenacion - Ernesto Lerma Carrasco");
            wr.close();
            bw.close();
        }catch(IOException e){};
    }
}
