/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sentencia_if_else;

/**
 *
 * 
 * Ernesto Lerma Carrasco
 */
public class Sentencia_If_Else {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int var= 5, var2= 10;
        if (var==5 && var2==10 ) 
        {
            System.out.println("Los numeros son 5 y 10 respectivamente.");
        }
        else
        {
            System.out.println("Los numeros no son 5 y 10 respectivamente.");
        }
    }
    
}
