/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases_objetos_metodos;

/**
 *
 * 
 * Ernesto Lerma Carrasco
 */

public class Carro {
    double peso;
    double altura;
    double ancho;
    double largo;
    int numeroPuertas;
    String modelo;
boolean encendido=true;

    public double getLargo() {
        return largo;
    }

    public void setLargo(double largo) {
        this.largo = largo;
    }

    public int getNumeroPuertas() {
        return numeroPuertas;
    }

    public void setNumeroPuertas(int numeroPuertas) {
        this.numeroPuertas = numeroPuertas;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }



    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }
    public void encender (){
        this.encendido=true;
        System.out.println("El carro esta encendido");
    }
     public void apagado (){
        this.encendido=false;
        System.out.println("El carro esta apagado");
    }
     public void estado (){
         if (this.encendido==true) 
         {
                   System.out.println("El carro esta encendido");
         }
         else
         {
                System.out.println("El carro esta apagado");
         }
     }
}
