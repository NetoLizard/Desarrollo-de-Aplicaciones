/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package switch_y_case;

/**
 *
 * 
 * Ernesto Lerma Carrasco
 */
public class Switch_Y_Case {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int x;
        x = 1;
        switch(x)
        {
            case 0:
            {
                System.out.println("Esta es la opcion 0.");
                break;
            }
            case 1:
            {
                System.out.println("Esta es la opcion 1.");
                break;
            }
            default:
            {
                System.out.println("Esta es la opcion por defecto.");
            }
        }
        
    }
    
}
